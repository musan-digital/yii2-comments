Installation

```
    'modules' => [
        'comments' => [
            'class' => 'lafa\modules\comments\Module',
        ],
        
        ...
    ]    
```

i18n
```        
    'components' => [
        'i18n' => [
            'translations' => [
                'lafa.comments' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@lafa/modules/comments/messages',
                ],
            ],
        ],
        
        ...
    ]   
```        