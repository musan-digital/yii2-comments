<?php

namespace lafa\modules\comments\controllers;

use lafa\modules\comments\models\Comment;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\TooManyRequestsHttpException;

class CommentController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionDetail()
    {
        return $this->render('detail');
    }

    public function actionCreate()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $lock_key = 'lock_comments_'.\Yii::$app->session->id;

        if (!\Yii::$app->cache->get($lock_key)) {
            \Yii::$app->cache->set($lock_key, true, 5);

            $comment = new Comment();

            $post = Yii::$app->request->post('Comment');
            if (!empty($post['guest_first_name'])) throw new NotFoundHttpException();

            if(Yii::$app->user->isGuest)
            {
                $comment->scenario = 'guest';
                $comment->user_id = null;
            }
            else{
                $comment->scenario = 'user';
                $comment->user_id = \Yii::$app->user->id;
            }

            if ($comment->load(Yii::$app->request->post()))
            {

                $parent = Comment::findOne(['id' => Yii::$app->request->post('parent_id')]);
                $comment->object = $parent->object;
                $comment->object_id = $parent->object_id;

                if($parent->isRoot())
                {
                    if ($comment->prependTo($parent)) {
                        if (!Yii::$app->request->isAjax) Yii::$app->response->redirect(['comments/comment/redirect', 'comment_id' => $comment->id], 301);
                        return ['status' => true];
                    }
                }
                else{
                    if ($comment->appendTo($parent)) {
                        if (!Yii::$app->request->isAjax) Yii::$app->response->redirect(['comments/comment/redirect', 'comment_id' => $comment->id], 301);
                        return ['status' => true];
                    }
                }

                Yii::$app->response->statusCode = 400;
                if (!Yii::$app->request->isAjax) Yii::$app->response->redirect(['comments/comment/redirect', 'comment_id' => $parent->id], 301);
                return ['status' => false, 'messages' => $comment->getErrors()];
            }

        }
        else throw new TooManyRequestsHttpException();

    }

    public function actionRedirect($comment_id)
    {

        $comment = Comment::findOne($comment_id);

        if (empty($comment)) Yii::$app->response->redirect(Url::home(), 301);

        $object = \Yii::createObject($comment->object)->findOne($comment->object_id);

        if (empty($object)) Yii::$app->response->redirect(Url::home(), 301);

        switch ($object->className()) {
            case News::className():
                $material = News::find()->where(['id' => $comment->object_id])->one();
                $url = Url::to(['news/detail', 'id' => $material->id, 'slug' => $material->slug, '#' => 'comment-' . $comment->id]);
                break;
//
//
//            case Video::className():
//                $video = Material::find()->where(['id' => $comment->object_id])->one();
//                $url = Url::to(['video/detail', 'id' => $video->id, 'slug' => $video->slug, '#' => 'comment-' . $comment->id]);
//                break;
            default:
                $url = Url::home();
        }
        Yii::$app->response->redirect($url, 301);
    }
}
