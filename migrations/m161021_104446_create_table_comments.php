<?php

use yii\db\Migration;

class m161021_104446_create_table_comments extends Migration
{
    public function up()
    {
        $this->createTable('comments', [
           'id' => $this->primaryKey(),
            'object' => $this->string(),
            'object_id' => $this->bigInteger(20),
            'user_id' => $this->bigInteger(20),
            'comment' => $this->text(),
            'is_active' => $this->boolean(),
            'create_date' => $this->dateTime(),
            'update_date' => $this->dateTime(),
            'tree' => $this->integer(),
            'lft' => $this->integer(),
            'rgt' => $this->integer(),
            'depth' => $this->integer(),
            'guest_name' => $this->string(),
            'guest_email' => $this->string(),
        ]);
    }

    public function down()
    {
        $this->dropTable('comments');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
