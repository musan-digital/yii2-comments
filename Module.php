<?php

namespace lafa\modules\comments;

/**
 * Comments module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'lafa\modules\comments\controllers';

    public $userClass = '';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
