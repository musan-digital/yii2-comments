<?php

namespace lafa\modules\comments\models;

use Yii;
use lafa\modules\comments\Module;
use lafa\modules\comments\models\CommentQuery;
use creocoder\nestedsets\NestedSetsBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "comments".
 *
 * @property integer $id
 * @property string $object
 * @property integer $object_id
 * @property integer $user_id
 * @property string $comment
 * @property integer $is_active
 * @property string $create_date
 * @property string $update_date
 * @property integer $tree
 * @property integer $lft
 * @property integer $rgt
 * @property integer $depth
 * @property string $guest_name
 * @property string $guest_email
 */
class Comment extends ActiveRecord
{

    public $parent_id;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comments';
    }

    public static function find()
    {
        return new CommentQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['object', 'object_id', 'comment'], 'required'],
            [['user_id'], 'required', 'on' => 'user'],
            [['guest_name'], 'required', 'on' => 'guest'],
            [['object_id', 'parent_id', 'is_active'], 'integer'],
            [['comment', 'parent_id', 'guest_name', 'guest_email'], 'safe'],
            [['object'], 'string', 'max' => 255],
            [['comment'], 'string'],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['user'] = ['user_id', 'comment'];
        $scenarios['guest'] = ['guest_name', 'guest_email', 'comment'];
        return $scenarios;

    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'comment' => Yii::t('lafa.comments', 'Текст комментария'),
            'comment_type' => Yii::t('lafa.comments', 'Тип коментарии'),
            'create_date' => Yii::t('lafa.comments', 'Создан'),
            'is_active' => Yii::t('lafa.comments', 'Отображение'),
            'update_date' => Yii::t('lafa.comments', 'Изменен'),
            'guest_name' => Yii::t('lafa.comments', 'Имя'),
            'guest_email' => Yii::t('lafa.comments', 'Email')
        ];
    }


    public function behaviors()
    {
        return [
            ['class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['create_date', 'update_date'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['update_date']
                ],
                'value' => new Expression('NOW()'),
            ],
            'tree' => [
                'class' => NestedSetsBehavior::className(),
                'treeAttribute' => 'tree',
                'leftAttribute' => 'lft',
                'rightAttribute' => 'rgt',
                'depthAttribute' => 'depth'
            ]
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }
    
    public function getParent()
    {
        return $this->parents(1)->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        $userClass = Module::getInstance()->userClass;
        return $this->hasOne($userClass::className(), ['id' => 'user_id']);
    }

    public function getName()
    {
        return empty($this->user) ? $this->guest_name : $this->user->fullName;
    }

}
