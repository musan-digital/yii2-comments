<?php
/**
 * Created by PhpStorm.
 * User: titanus
 * Date: 10.03.16
 * Time: 17:58
 */

namespace lafa\modules\comments\models;



use creocoder\nestedsets\NestedSetsQueryBehavior;
use yii\db\ActiveQuery;

class CommentQuery extends ActiveQuery
{
    public function behaviors() {
        return [
            NestedSetsQueryBehavior::className(),
        ];
    }

    public function getComment()
    {
        $this->andWhere(['>', 'depth', 0])->orderBy('tree, lft, create_date DESC');
        return $this;
    }

}