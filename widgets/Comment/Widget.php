<?php

/**
 * Created by PhpStorm.
 * User: titanus
 * Date: 10.03.16
 * Time: 15:57
 */

namespace lafa\modules\comments\widgets\Comment;

use yii\db\ActiveRecord;
use lafa\modules\comments\models\Comment;

class Widget extends \yii\base\Widget
{
    /**
     * @var ActiveRecord
     */
    public $model;

    public function run()
    {
        \lafa\modules\comments\widgets\Comment\assets\CommentAssets::register($this->view);

        $rootComment = Comment::findOne(['object' => $this->model->className(), 'object_id' => $this->model->id, 'depth' => 0]);
        if(empty($rootComment)) {
            $rootComment = new Comment([
                'object' => $this->model->className(),
                'object_id' => $this->model->id,
                'is_active' => 1,
                'user_id' => 0,
                'comment' => 'ROOT',
            ]);
            $rootComment->makeRoot();
        }

        $query = $rootComment->leaves()->where([
                'object' => $this->model->className(),
                'object_id' => $this->model->id
            ])->andWhere(['<>', 'depth', '0'])->orderBy('tree, lft, create_date DESC');

        $comments = $query->all();


        $comment_form = new Comment();
        $comment_form->parent_id = $rootComment->id;

        return $this->render('comments', [
            'comments' => $comments,
            'comment_form' => $comment_form,
        ]);
    }

}