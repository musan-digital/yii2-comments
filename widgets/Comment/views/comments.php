<?php \yii\widgets\Pjax::begin(['id' => 'comments', 'timeout' => 10000]); ?>
<div class="b-comments" id="comment<?= $comment_form->parent_id ?>">
    <h2 class="b-comments__h2"><?= count($comments) ?> <?= Yii::t('lafa.comments','{n, plural, =0{Комментариев} =1{Комментарий} one{Комментарий} few{Комментария} many{Комментариев} other{Комментарие}}' , array(
            'n' => count($comments),
        )); ?></h2>

    <a name="comment-<?= $comment_form->parent_id ?>"></a>
    <?php foreach($comments as $comment): ?>
        <?= $this->render('@lafa/modules/comments/widgets/Comment/views/_item.php', ['comment' => $comment]) ?>
    <?php endforeach; ?>
    
    <?= $this->render('_form', ['comment_form' => $comment_form]) ?>

    <div class="comment__item">
        <a style="display:none;" href="javascript:void(0)" class="comment__answer-link js-comments-btn" id="comment-root" data-id="<?= $comment_form->parent_id ?>" data-root="1"><?= Yii::t('lafa.comments', 'Написать комментарий'); ?></a>
    </div>
</div>
<?php \yii\widgets\Pjax::end(); ?>
