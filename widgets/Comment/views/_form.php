<?php

use mitrii\attachments\helpers\Image;
use yii\helpers\Html;

/**
 * @var $form \yii\widgets\ActiveForm
 */

?>
<?php $form = \yii\widgets\ActiveForm::begin([
    'id' => 'comment-form',
    'action' => \yii\helpers\Url::to(['comments/comment/create']),
    'options' => [
        'class' => 'b-form js-b-form'
    ]
]) ?>
<?= yii\helpers\Html::hiddenInput('parent_id', $comment_form->parent_id, ['id' => 'comment-parent-id', 'data-root_id' => $comment_form->parent_id]) ?>
    <div class="b-form__item">
        <div class="b-form__flex">
            <div class="comment__item--input" style="display: none">
                <?= Html::textInput(Html::getInputName($comment_form, 'guest_first_name')) ?>
            </div>
            <?= $form->field($comment_form, 'guest_name')->label(false)->textInput([
                'placeholder'=>Yii::t('lafa.comments', 'Имя'),
                'class' => 'b-form__input',
                'id' => 'form-name'
            ]); ?>
            <?= $form->field($comment_form, 'guest_email')->label(false)->textInput([
                'placeholder'=>Yii::t('lafa.comments', 'Email'),
                'class' => 'b-form__input',
                'id' => 'form-email'
            ]); ?>
        </div>
    </div>

    <div class="b-form__item">
        <?= $form->field($comment_form, 'comment')->label(false)->textarea([
            'class'=>'add-answer add-answer--comment b-form__textarea',
            'placeholder'=>Yii::t('lafa.comments', 'Напишите комментарий'),
            'id' => 'firm0',
            'cols' => 30,
            'rows' => 10
        ]); ?>
    </div>

    <div class="b-form__item">

        <button type="submit" class="b-comments__btn"><?= Yii::t('lafa.comments', 'Отправить') ?></button>
    </div>

<?php \yii\widgets\ActiveForm::end() ?>