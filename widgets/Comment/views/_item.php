<?php
/**
 * Created by PhpStorm.
 * User: titanus
 * Date: 10.03.16
 * Time: 17:38
 */

use lafa\helpers\Format;
use mitrii\attachments\helpers\Image;
?>

<div class="b-comments__items depth-<?= $comment->depth ?> <?php echo ($comment->depth > 1) ? 'b-comments__items_answer' : ''?>" id="comment<?= $comment->id ?>">
    <a name="comment-<?= $comment->id ?>"></a>
    <span class="b-comments__name"><?= $comment->name; ?></span><span class="b-comments__data"><?= Yii::$app->getFormatter()->asDate($comment->create_date, 'd MMMM, Y'); ?></span>
    <div class="b-comments__anons"><?= $comment->comment; ?></div>
        <button type="button" class="b-comments__btn js-comments-btn" data-id="<?= $comment->id ?>"><?= Yii::t('lafa.comments', 'Ответить') ?></button>
</div>

