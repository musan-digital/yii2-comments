<?php
/**
 * Created by PhpStorm.
 * User: titanus
 * Date: 17.10.16
 * Time: 12:01
 */

namespace lafa\modules\comments\widgets\Comment\assets;


class CommentAssets extends \yii\web\AssetBundle
{
    public $sourcePath = '@lafa/modules/comments/widgets/Comment/assets';
    public $js = [
        'js\comments.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}