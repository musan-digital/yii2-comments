$(document).on('pjax:success ready', (function(){

    $('#comment-form').submit(function (event) {
        event.preventDefault();

        var form = $(this);
        if ( form.data('requestRunning') ) {
            return;
        }
        form.data('requestRunning', true);
        $('#comment-submit').prop('disabled', true);

        var data = $("#comment-form :input").serializeArray();

        $.post(
            $("#comment-form").attr('action'),
            data,
            function (result) {
                $('#comment-submit').prop('disabled', false);
                if (result.status)
                {
                    form.data('requestRunning', false);
                    $.pjax.reload({container:"#comments"})
                }
            },
            'JSON'
        );
    });

    $('.js-comments-btn').click(function (event) {
        var id = $(this).data('id');

        $('#comment-form').appendTo($('#comment'+id));
        $('#comment-parent-id').val(id);

        if ($(this).data('root'))
            $('#comment-root').hide();
        else
            $('#comment-root').show();
    });
}));

